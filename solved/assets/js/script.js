// TODO: Create a variable to hold the count

let count = 0;

console.log(count);

// TODO: Create variables to select the increment button, decrement button and count heading by their respective ids.

let increment = document.querySelector("#increment");
let decrement = document.querySelector("#decrement");
let pageCount = document.querySelector("#count");

// TODO: Create a function that displays the current count on the page

const currentCount = () => {
    pageCount.textContent = count;
}

// TODO: Create an addEventListener that will increment the count on click
// and calls the function to display the current count

const incrementCount = (event) => {
    count++;
    currentCount();
}

increment.addEventListener("click", incrementCount);

// TODO: Create an addEventListener that will decrement the count on click
// and calls the function to display the current count

const decrementCount = (event) => {
    count--;
    if (count < 0) {
        count = 0;
    }
    currentCount();
}

decrement.addEventListener("click", decrementCount);
